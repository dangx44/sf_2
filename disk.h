#ifndef DISK_H
#define DISK_H

#define DISK_BLOCK_SIZE 4096

int  disk_init( const char *filename, int nblocks );
/**
 * disk_init  - Create a virtual disk file
 * @filename: Name of the virtual disk file
 * nblocks: Block count
 *
 * Create a virtual disk of size @nblocks: x DISK_BLOCK_SIZE bytes, as a single file
 * named @filename: on the host computer.
 *
 * Return: -1 if @filename: is invalid or if the virtual disk file cannot be
 * created or truncated to the right size. 0 otherwise.
 */

int  disk_size();
/**
 * block_disk_count - Get disk's block count
 *
 * Return: the number of blocks that the currently open disk contains.
 */

void disk_read( int blocknum, char *data );
/**
 * disk_read - Read a block from disk
 * @blocknum: Index of the block to read from
 * @buf: Data buffer to be filled with content of block
 *
 * Read the content of virtual disk's block @blocknum: (DISK_BLOCK_SIZE bytes) into
 * buffer @data:.
 *
 * Return: Error Msg if @blocknum: is out of bounds or inaccessible, or if the reading
 * operation fails.
 */

void disk_write( int blocknum, const char *data );
/**
 * disk_write - Write a block to disk
 * @blocknum: Index of the block to write to
 * @data: Data buffer to write in the block
 *
 * Write the content of buffer @data: (DISK_BLOCK_SIZE bytes) in the virtual disk's
 * block @blocknum:.
 *
 * Return: Error Msg if @blocknum: is out of bounds or inaccessible or if the writing
 * operation fails.
 */

void disk_close();
/**
 * Close the virtual disk file
 */


#endif